/*
ASBA

File: asba_op.c
Author: Alexjan Carraturo
Email: alexjan.carraturo@gmail.com (but for insults and complains you could write at ....)
License: see LICENSE.txt

*/

#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#include <string.h>


#include "asba.h"


/* 
    I'm writing this function because I'm lost in my own code. 
    Said that, I don't know if it will do what is suppose to do.
    This mean that old fashion print function could actually be
    really useful. This is not meant to stay.  
*/
void asba_debug_descriptor(sAsba_desc_t *desc)
{
    fprintf(stderr," Name: %s\t Generator: %s\n Blocksize %u\t Blocknumber: %u\t Data offset: %lu\t Special Blocks %u\n used %u\t current %u Status: %d\n",
                    desc->hdesc->name, desc->hdesc->generator, desc->hdesc->bs,
                     desc->hdesc->bn, desc->data_offset, desc->hdesc->spblk, 
                     desc->hdesc->used, desc->curr, desc->status);
}




#ifdef MULTI_MAP
sAsba_desc_t* asba_open_archive(sAsba_desc_t *asba_desc, char *asba_name, int flags)
{
    ASBA_DBG("%s\t %d\n", __func__, __LINE__);
    //asba_desc=malloc(sizeof(sAsba_desc_t));
    if ( asba_desc !=NULL )
    {
        asba_desc->status|=DESC_STATUS_INIT;
        asba_desc->fd=open(asba_name, O_RDWR); 
        if (asba_desc->fd > 0 )
        {
            asba_desc->status|=DESC_STATUS_OPEN;
            //tmp=read(asba_desc->fd, (void*)&asba_desc->hdesc, sizeof(sAsba_header_t));
            asba_desc->hdesc=(sAsba_header_t*)mmap(NULL, sizeof(sAsba_header_t), PROT_READ|PROT_WRITE, MAP_SHARED, asba_desc->fd, 0);
            if(asba_desc->hdesc != NULL)
            {
                asba_desc->status|=DESC_STATUS_READ;
                //asba_desc->tdesc.usef=malloc((size_t)asba_desc->hdesc.bn);
                //tmp=read(asba_desc->fd, (void*)asba_desc->tdesc.usef, asba_desc->hdesc.bn);
                //This table need mapping, no malloc
                asba_desc->tdesc.usef=(uint8_t*)mmap(NULL, (size_t)(asba_desc->hdesc->bn), PROT_READ|PROT_WRITE, MAP_SHARED, asba_desc->fd, sizeof(sAsba_header_t));

                if( asba_desc->tdesc.usef!=NULL )
                {
                    asba_desc->status|=DESC_STATUS_TAB;
                    asba_desc->data_offset=( sizeof(sAsba_header_t)+asba_desc->hdesc->bn );
                    asba_desc->map=mmap(NULL, (size_t)(asba_desc->hdesc->bn * asba_desc->hdesc->bs), PROT_READ|PROT_WRITE, MAP_SHARED, asba_desc->fd, asba_desc->data_offset);
                    if( asba_desc->map !=NULL)
                    {
                        asba_desc->status|=DESC_STATUS_MAP;
                        asba_desc->curr=0;
                    }
                }
            }
        }
    }
    
    return asba_desc;


}


#else

size_t asba_full_arch_size(char *asba_name)  
{
    // opening the file in read mode
    FILE* fp = fopen(asba_name, "r");
  
    // checking if the file exist or not
    if (fp == NULL) {
        ASBA_ERR("File Not Found!\n");
        return -1;
    }
  
    fseek(fp, 0L, SEEK_END);
  
    // calculating the size of the file
    long int res = ftell(fp);
  
    // closing the file
    fclose(fp);
  
    return (size_t)res;
}


void asba_read_header(sAsba_header_t *header, char *asba_name)
{
    int fd;

    fd=open(asba_name, O_RDONLY);
    read(fd, (void*)header, sizeof(sAsba_header_t));
    close(fd); 
}

uint64_t asba_calculate_archsize(sAsba_header_t *header)
{
    return (uint64_t)((header->bs * header->bn) + header->bn + sizeof(sAsba_header_t));
}

void asba_init_descriptor(sAsba_desc_t *asba_desc)
{
    asba_desc->hdesc=(sAsba_header_t*)asba_desc->fmap;
    asba_desc->tdesc.usef=(uint8_t*)(asba_desc->fmap+sizeof(sAsba_header_t));
    asba_desc->map=(void*)(asba_desc->fmap+(sizeof(sAsba_header_t)+asba_desc->hdesc->bn)); 
    asba_desc->status|=(DESC_STATUS_READ|DESC_STATUS_TAB|DESC_STATUS_MAP);   
    asba_desc->data_offset=( sizeof(sAsba_header_t)+asba_desc->hdesc->bn );
    asba_desc->curr=asba_desc->hdesc->spblk+1;
}

int asba_check_arcsize(char* asba_name, int *arch_size)
{
    int status=STATUS_OK;
    int est_size=0;
    sAsba_header_t theader;
       
    *arch_size=asba_full_arch_size(asba_name);
    asba_read_header(&theader, asba_name);
    est_size=(int)asba_calculate_archsize(&theader);

    if( *arch_size != est_size )
    {
        ASBA_ERR("WARNING: Estimated file size %d, real file size %d\n", est_size, *arch_size );
        status=GENERIC_FILE_ERROR;
    }

    return status; 
}

int asba_descriptor_setup(sAsba_desc_t *asba_desc, char *asba_name, int arch_size)
{
    int status=STATUS_OK;

    asba_desc->status|=DESC_STATUS_INIT;
     
    asba_desc->fd=open(asba_name, O_RDWR); 
    if (asba_desc->fd > 0 )
    {
        asba_desc->status|=DESC_STATUS_OPEN;
        asba_desc->fmap=(void*)mmap(NULL, arch_size, PROT_READ|PROT_WRITE, MAP_SHARED, asba_desc->fd, 0);
        if(asba_desc->fmap != NULL)
        {
            asba_init_descriptor(asba_desc);    
        }
        else 
        {
            status=GENERIC_MEM_ERROR;
            asba_desc->status=DESC_STATUS_ERR;
        }
    }
    else
    {
        status=GENERIC_FILE_ERROR;
        asba_desc->status=DESC_STATUS_ERR;
    }
    return status;
}

int asba_open_check_parameter(sAsba_desc_t *asba_desc, char *asba_name, int *arch_size)
{
    int status=STATUS_OK;

    if( asba_name == NULL)
        status=GENERIC_PARAM_ERROR;
    else
    {
        if ( asba_desc==NULL )
            status=GENERIC_PARAM_ERROR;
        else
            status=asba_check_arcsize(asba_name, arch_size); 
    }
    return status;
}


sAsba_desc_t* asba_open_archive(sAsba_desc_t *asba_desc, char *asba_name, int flags)
{
    int status=STATUS_OK;
    int arch_size=0;
    
    if((status=asba_open_check_parameter(asba_desc, asba_name, &arch_size)) == STATUS_OK )
        status=asba_descriptor_setup(asba_desc, asba_name, arch_size);
    else
    {
        return NULL;   
    }
    //asba_debug_descriptor(asba_desc);
    return asba_desc;
}
#endif


#ifdef MULTI_MAP
int asba_close_archive(sAsba_desc_t *asba_desc)
{
    munmap(asba_desc->map, (asba_desc->hdesc->bn * asba_desc->hdesc->bs));
    munmap(asba_desc->tdesc.usef, (size_t)(asba_desc->hdesc->bn));
    munmap(asba_desc->hdesc, sizeof(sAsba_header_t));
    close(asba_desc->fd);

    asba_desc->status=DESC_STATUS_NULL;
    return asba_desc->status;
}
#else
int asba_close_archive(sAsba_desc_t *asba_desc)
{
    munmap(asba_desc->fmap, ((asba_desc->hdesc->bn * asba_desc->hdesc->bs)+asba_desc->hdesc->bn+sizeof(sAsba_header_t)));
    close(asba_desc->fd);

    asba_desc->status=DESC_STATUS_NULL;
    return asba_desc->status;
}

#endif





/*  Creation of new archive: split in multiple functions
    PROBLEM: The final number of block usable available will be less than requested
    Some will be used as superblock
*/

int asba_format_archive(int fd, sAsba_header_t *header, sAsba_btab_status_t *blocktab)
{
    int i;
    uint8_t *empty_block;
    empty_block=malloc(header->bs);
    memset((void*)empty_block, EMPTYBLK_VAL, header->bs);

    write(fd, (void*)header, sizeof(sAsba_header_t));
    write(fd, (void*)blocktab->usef, header->bn);
    for(i=0;i<header->bn;i++)
    {
        write(fd, (void*)empty_block, header->bs);
    }
    /* To be completed*/
    free(empty_block);

    /*Error management missing */
    return STATUS_OK;


}

int asba_init_archive(int fd, sAsba_header_t *header, uint32_t sb_szbyte)
{
    sAsba_btab_status_t   blocktab;
    
    blocktab.usef=malloc(sizeof(header->bn));
    //memset(blocktab.usef, BTAB_STATUS_INIT, header->bn);
    if(sb_szbyte > 0 )
    {
        header->spblk=(uint16_t)asba_block_to_spblock(header, sb_szbyte);
        asba_init_spblock_arch(header, &blocktab, header->spblk );  
    }
    return asba_format_archive(fd, header, &blocktab);
}


int asba_create_archive_check_param(sAsba_header_t *header, char *name, char *generator, uint32_t blockno, uint32_t blocksize)
{
    int status=STATUS_OK;

    if( generator != NULL )
        strncpy(header->generator, generator, strlen(generator));
    else
        strcpy(header->generator,"NOTSPECIFIED\0");

    if( (blocksize <= 0) || ( blockno <= 0))
        status=GENERIC_PARAM_ERROR;  
    
    return status;
}


void asba_init_header(sAsba_header_t *header, char *name, uint32_t blocksize, uint32_t blockno)
{
    char el='\0';
    
    strncpy(header->name, name, strlen(name));
    strncat(header->name, &el, 1);
    header->bs=blocksize;
    header->bn=blockno;
    header->used=0;
    header->magic=MAGIC;
}



int asba_create_new_archive(char *name, int *fd)
{
    int status=STATUS_OK;
    if( access( name, F_OK ) == 0 ) 
    {
        status = GENERIC_FILE_ERROR;
        ASBA_ERR("File %s already exist\n", name);
    }
    else
    {
        *fd=open(name, O_RDWR | O_CREAT, 0644 );
        if( *fd <= 0 )
        {
            status=GENERIC_FILE_ERROR;
            ASBA_ERR("Impossible to open %s\n", name);
        }       
    }
    return status;
} 

// The spblk_sz header is for the sizeof() of the superblock structure 

int asba_create_archive(char *name,            //filename 
                        char *generator,        //application that generated the file
                        uint32_t blocksize,     // Size of the single block in bytes
                        uint32_t blockno,       // Number of blocks
                        uint16_t spblk_sz)      // Number of bytes dedicated to special block
{
    int fd, status=STATUS_OK;
    sAsba_header_t header;
    
     if((status=asba_create_archive_check_param(&header, name, generator,blockno, blocksize)) == STATUS_OK)
     {
         asba_init_header(&header,name,blocksize, blockno);
         if((status=asba_create_new_archive(name, &fd)) == STATUS_OK)
             status=asba_init_archive(fd,&header, spblk_sz);
     }   
    return status;
}

int asba_delete_archive()
{
    /* TODO: Although, you could just erase the file, saving me a lot of time in coding */
    return 0;
}

/*int asba_format_archive()
{
    // TODO: For the moment erase and create one new. 
   return 0; 
}*/


int resize_archive()
{
    /*TODO: Possibly, but no likely. I mean, have you any Idea on how much time it will ask?
    I mean... I'm not saying that I have a "life to live", but I'm trying. So, for these wait until the
    next mass lockdown... or quarantine. Or, and this is quite an idea, you could do it; 
    don't worry, I'm not jelous of my code. You could implement it if you don't touch the 
    other part of the source code. What do you think about?
    */
    return 0;
}




