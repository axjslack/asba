#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>

#include "asba.h"


void DBG_dump_memarea(void *mem, size_t size, char *text)
{
    unsigned char *p=NULL;

    p=(unsigned char*)mem;
    ASBA_DBG("%s\t%d\t Printing %s at %p  size %lu\n",__func__, __LINE__,text, (void*)p,size );

    ASBA_DBG("%s\t%d\t Dumping memory area\n", __func__, __LINE__);
    for(int i=0;i<size;i++)
    {
        if((i%16) == 0)
	    ASBA_DBG("\n");
        ASBA_DBG(" %2hhx ", *p);
        p++;
    }
    ASBA_DBG("\n%s\t%d\t END Dumping memory area\n", __func__, __LINE__);

}   


/*This should take the size of data structure of the superblock and 
calculate number of blocks)*/
uint16_t asba_block_to_spblock(sAsba_header_t *head ,uint32_t sb_szbyte)
{
    int sb_szblk=0;
    sb_szblk=sb_szbyte/head->bs;
    if((sb_szblk * head->bs) < sb_szbyte)
        sb_szblk++;
 
    if(sb_szblk > MAX_SPEC_BLOCK)
        sb_szblk= MAX_SPEC_BLOCK;
    return (uint16_t)sb_szblk;
}


void asba_init_spblock_arch(sAsba_header_t *head, sAsba_btab_status_t *btab, uint16_t sb_szblk)
{
    for(int i=0;i<sb_szblk;i++)
        btab->usef[i]|=(BTAB_STATUS_SPEC|BTAB_STATUS_FULL);

    head->used+=sb_szblk;
    head->spblk=sb_szblk;
}

/* Special Block or super block (I have not decided yet) is seen as
 an entity composed by multiple block/
 How this could be done is still a mistery, When I'll figure it out
 I'll let you know.  */

int asba_read_sblock(sAsba_desc_t *desc,void *dest_buffer )
{
    void *sp_add=NULL;
    size_t sp_size=0;

    sp_add=asba_get_block_address(desc, 0);
    sp_size=(size_t)(desc->hdesc->bs * desc->hdesc->spblk);
    memcpy(dest_buffer,sp_add, sp_size);
    return STATUS_OK;
}


int asba_write_sblock(sAsba_desc_t *desc, void *src_buffer)
{
    uint8_t *verify;
  
    memcpy( asba_get_block_address(desc, 0), src_buffer, 
      (size_t)(desc->hdesc->bs * desc->hdesc->spblk) );
    for(int i=0;i<desc->hdesc->spblk;i++)
    {
        desc->tdesc.usef[desc->curr]|=BTAB_STATUS_FULL;
    }
    msync(desc->map, (desc->hdesc->bs * desc->hdesc->bn), MS_ASYNC );
    
    /*Verification of correct writing of sb*/
    verify=malloc(desc->hdesc->bs * desc->hdesc->spblk);
    asba_read_sblock(desc, (void*)verify);
    if( memcmp((void*)verify, (void*)src_buffer, desc->hdesc->bs * desc->hdesc->spblk))
    {
        ASBA_ERR("%s\t %d\t Block size %u\tSpecial Blocks %u\n", __func__, __LINE__,desc->hdesc->bs,desc->hdesc->spblk);
        ASBA_ERR("%s\t %d\t Verification of writing on sb failed\n", __func__, __LINE__);
       
    }

    free(verify);
    return STATUS_OK;
    /*Have you ever had that burning feeling to have forgotten something?
    I do. In this moment, but I can't remember what it is.*/
}



// Old proctected block read/write

int asba_read_proct_block(sAsba_desc_t *desc, void *dest_buffer)
{
    int status=STATUS_OK;
    if ((desc->tdesc.usef[desc->hdesc->spblk] & (BTAB_STATUS_PROCT|BTAB_STATUS_SPEC)) == (BTAB_STATUS_SPEC|BTAB_STATUS_PROCT))
    {
        if((status=asba_check_descriptor(desc) ) == STATUS_OK)
        {
            if(asba_is_archive_empty(desc))
                status=ERROR_READ_EMPTY;
            else
               status=asba_read_block(desc, dest_buffer,desc->hdesc->spblk); 
        }
    }
    else
        status=ERROR_NO_SPEC;

    return status;
}

int asba_write_proct_block(sAsba_desc_t *desc, void *src_buffer)
{
    int status=STATUS_OK; 
    
    if ((desc->tdesc.usef[desc->hdesc->spblk] & (BTAB_STATUS_PROCT|BTAB_STATUS_SPEC)) == (BTAB_STATUS_SPEC|BTAB_STATUS_PROCT))
    {
        if((status=asba_check_descriptor(desc) ) == STATUS_OK)
        {
            if(asba_is_archive_full(desc))
                status=ERROR_WRITE_FULL;
            else
                status=asba_write_block(desc, src_buffer,desc->curr); 
        }
    }
    else
        status=ERROR_NO_SPEC;

   return status;
}