/* This is the ASBA: Another (or Alexjan, depending by the waether) Small Block Archive 

It likely that many things here will be referred as asba instead of asba. This is due to the
fact that I usually not so good in naming (luckily I don't have children) and at the beginning
this library was called "Block Archive", shortened asba.

Now, I know that I could change every reference to asba, but it is really worthy of my time?
I mean, "A rose by any other name would smell as sweet" (W. Shakespeare). 

If you will take a look to the code you will notice immediately that there are more urgent matter
to solve.

*/


#ifndef _ASBA_H_
#define _ASBA_H_

#include <stdint.h>
#include <stdio.h>

#define     MAGIC                           0x55

#define     STATUS_OK                        0
#define     GENERIC_ERROR                   -1
#define     GENERIC_MEM_ERROR               -2
#define     GENERIC_FILE_ERROR              -4
#define     GENERIC_PARAM_ERROR             -8

#define     ERROR_READ_EMPTY                -16
#define     ERROR_WRITE_FULL                -32
#define     ERROR_NOBLOCK                   -64
#define     ERROR_NO_SPEC                   -128

//Value for descriptor status
#define     DESC_STATUS_NULL        0x00000000
#define     DESC_STATUS_INIT        0x00000001
#define     DESC_STATUS_OPEN        0x00000002
#define     DESC_STATUS_READ        0x00000004
#define     DESC_STATUS_TAB         0x00000008
#define     DESC_STATUS_MAP         0x00000010
#define     DESC_STATUS_ERR         0x00000080


//Values for Block table
#define     BTAB_STATUS_EMPTY             0x00
#define     BTAB_STATUS_FULL              0x01
#define     BTAB_STATUS_PROCT             0x02
#define     BTAB_STATUS_SPEC              0x04
#define     BTAB_STATUS_INIT              0x80

#define     TRUE                    0x01
#define     FALSE                   0x00

#define     DEF_SPEC_BLOCK          0x00

#define     MAX_SPEC_BLOCK          0xFFFF
#define     EMPTYBLK_VAL            0x00



#define ASBA_ERR(...) fprintf(stderr, __VA_ARGS__);
#ifdef DBG
#define ASBA_DBG(...) fprintf(stderr, __VA_ARGS__);
#else
#define ASBA_DBG(...) 
#endif


typedef struct sAsba_header {
    char name[16];
    char generator[16];
    uint64_t            gtime; //generation time
    uint64_t            ltr; // Last time read
    uint64_t            ltw; // Last time write
    uint32_t            bs; // size of block in bytes
    uint32_t            bn; // number of blocks
    uint32_t            used; // Number of block used
    uint16_t            spblk; //Special block
    uint8_t             pad; //Pad for future use
    uint8_t             magic; // Identify the block archive header end
} sAsba_header_t;


typedef struct sAsba_btab_status {
    uint8_t             *usef;  //Use flag
} sAsba_btab_status_t;


typedef struct sAsba_desc {
    sAsba_btab_status_t   tdesc; //table descriptor
    uint64_t               data_offset; //Data offset in bytes
    int                    status; //Status of descriptor
    int                    fd;     //Real file descriptor
    uint32_t               curr; // Current block
    void                   *map; //Pointer to block file map
    void                   *fmap; //full file map
    sAsba_header_t        *hdesc; //header descriptor
} sAsba_desc_t;


// Fuctions prototypes 

//Archive Operations
void asba_debug_descriptor(sAsba_desc_t *desc);
sAsba_desc_t* asba_open_archive(sAsba_desc_t *asba_desc, char *asba_name, int flags);
int asba_close_archive(sAsba_desc_t *asba_desc);
int asba_init_archive(int fd, sAsba_header_t *header, uint32_t sb_szbyte);
int asba_create_archive(char *name, char *generator, uint32_t blocksize, uint32_t blockno, uint16_t spblk_sz);

//Block Operations
int asba_is_archive_full(sAsba_desc_t *desc);
int asba_is_archive_empty(sAsba_desc_t *desc);
int asba_check_descriptor(sAsba_desc_t *desc);
int asba_read_current_block(sAsba_desc_t *desc, void *dest_buffer);
int asba_write_current_block(sAsba_desc_t *desc, void *src_buffer);
int asba_delete_block(sAsba_desc_t *desc, uint32_t blockno);
void *asba_get_block_address(sAsba_desc_t *desc, uint32_t blockno);
int asba_read_block(sAsba_desc_t *desc, void *dest_buffer, uint32_t blockno);
int asba_write_block(sAsba_desc_t *desc, void *src_buffer, uint32_t blockno);

// Special blocks operations 
uint16_t asba_block_to_spblock(sAsba_header_t *head ,uint32_t sz);
void asba_init_spblock_arch(sAsba_header_t *head, sAsba_btab_status_t *btab, uint16_t sz);
int asba_write_sblock(sAsba_desc_t *desc, void *src_buffer);
int asba_read_sblock(sAsba_desc_t *desc,void *dest_buffer );

// Protected blocks operations
int asba_read_proct_block(sAsba_desc_t *desc, void *dest_buffer);
int asba_write_proct_block(sAsba_desc_t *desc, void *src_buffer);

// Debug
void DBG_dump_memarea(void *mem, size_t size, char *text);

#endif //_ASBA_H_
