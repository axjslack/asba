/*
ASBA

File: test.c
Author: Alexjan Carraturo
Email: alexjan.carraturo@gmail.com (but for insults and complains you could write at ....)
License: see LICENSE.txt

*/

#include <stdio.h>
#include <stdlib.h>


#include "asba.h"



void test_block_write(sAsba_desc_t *desc)
{
    uint8_t testblock[4];

    testblock[3]=0x01;
    testblock[2]=0x02;
    testblock[1]=0x03;
    testblock[0]=0x04;

    ASBA_DBG("%s\t %d\n", __func__, __LINE__);
    asba_write_current_block(desc, testblock);
    ASBA_DBG("%s\t %d\n", __func__, __LINE__);

}


void test_block_delete(sAsba_desc_t *desc)
{
    asba_delete_block(desc, 0);
}

void test_block_read(sAsba_desc_t *desc)
{
    uint32_t testb;

    ASBA_DBG("%s\t %d\n", __func__, __LINE__);
    asba_read_current_block(desc, &testb);
    ASBA_DBG("%s\t %d Read from block %u\n", __func__, __LINE__, testb);


}

void test_archive(sAsba_desc_t *desc)
{
    ASBA_DBG("%s\t %d\n", __func__, __LINE__);    
    asba_create_archive("pippo.ba", "testapp",4, 8, (BTAB_STATUS_SPEC|BTAB_STATUS_PROCT));
    ASBA_DBG("%s\t %d\n", __func__, __LINE__);    
    asba_open_archive(desc, "pippo.ba", 0);
    ASBA_DBG("%s\t %d\n", __func__, __LINE__); 
    asba_debug_descriptor(desc);
    ASBA_DBG("%s\t %d\n", __func__, __LINE__); 
    test_block_write(desc);
    test_block_write(desc);
    test_block_read(desc);
    test_block_write(desc);
    test_block_delete(desc);

    ASBA_DBG("%s\t %d\n", __func__, __LINE__); 
    asba_close_archive(desc);
}



int main()
{
    sAsba_desc_t desc;

    ASBA_DBG("%s\t %d\n", __func__, __LINE__);    
    test_archive(&desc);
    

}