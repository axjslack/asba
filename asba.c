/*
ASBA

File: asba.c
Author: Alexjan Carraturo
Email: alexjan.carraturo@gmail.com (but for insults and complains you could write at ....)
License: see LICENSE.txt

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>

#include "asba.h"


/* Function used by read/write operation
 * This will not be exported   
*/

void *asba_get_block_address(sAsba_desc_t *desc, uint32_t blockno)
{
    return (void*)(desc->map+(desc->hdesc->bs * blockno));
}

void *asba_get_current_block_address(sAsba_desc_t *desc)
{
    return asba_get_block_address(desc, desc->curr);
}


int asba_is_archive_full(sAsba_desc_t *desc)
{
    if( desc->hdesc->used == (desc->hdesc->bn -1))
        return TRUE;
    else
        return FALSE;
}

int asba_is_archive_empty(sAsba_desc_t *desc)
{
    if( desc->hdesc->used == 0)
        return TRUE;
    else
        return FALSE;
}


int asba_check_descriptor(sAsba_desc_t *desc)
{
    int status=STATUS_OK;
    if(desc == NULL)
    {
        status=GENERIC_MEM_ERROR;
    }
    else
    {
        ASBA_DBG("%s\t %d:\t Status value %d \n", __func__, __LINE__, desc->status);
         ASBA_DBG("%s\t %d:\t Logic value to be compared %d \n", __func__, __LINE__,
         DESC_STATUS_INIT|DESC_STATUS_OPEN|DESC_STATUS_READ|DESC_STATUS_TAB|DESC_STATUS_MAP);
        if (desc->status != (DESC_STATUS_INIT|DESC_STATUS_OPEN|DESC_STATUS_READ|DESC_STATUS_TAB|DESC_STATUS_MAP))
        {
            status=GENERIC_FILE_ERROR;
        }
    }
    return status;

}


void asba_update_current_block(sAsba_desc_t *desc, uint32_t blockno)
{
    if(blockno >= (desc->hdesc->bn -1))
        desc->curr=0;
    else
    {
        desc->curr=blockno;
        desc->curr++;  
    }
}

int32_t asba_search_next_free_block2(sAsba_desc_t *desc, uint8_t flag)
{
    uint32_t counter=desc->hdesc->bn;
    int32_t block=-1;
    int32_t i=0;

    while (counter > 0)
    {
        if (!((desc->tdesc.usef[i] & BTAB_STATUS_PROCT) ||(desc->tdesc.usef[i] & BTAB_STATUS_SPEC) || (desc->tdesc.usef[i] & BTAB_STATUS_FULL) ) )
        {
            block=i;
            break;
        }
        else
        {
            counter--;
            i++;
        } 
    }
    ASBA_DBG("%s\t %d\n", __func__, __LINE__);
    return block;
}


int asba_search_next_block(sAsba_desc_t *desc, uint8_t flag)
{
    int status=STATUS_OK;
    uint32_t p=desc->curr;
    uint32_t counter=desc->hdesc->bn;

    //Logic to be improved 
    while(( (desc->tdesc.usef[p] & (BTAB_STATUS_PROCT|BTAB_STATUS_SPEC|BTAB_STATUS_FULL) ) != flag) 
        && (counter > 0 ))
    {  
        if( p >= (desc->hdesc->bn -1) )
            p=0;
        else
            p++;
        counter--;           
    }
    if( status == STATUS_OK)
        desc->curr=p;

    return status;
}

int asba_search_next_free_block(sAsba_desc_t *desc)
{
    if(desc->hdesc->used == (desc->hdesc->bn -1))
        return ERROR_WRITE_FULL;
    else
        return asba_search_next_block(desc, BTAB_STATUS_EMPTY);
}

int asba_search_next_used_block(sAsba_desc_t *desc)
{
    if(desc->hdesc->used == (desc->hdesc->bn -1))
        return ERROR_WRITE_FULL;
    else
        return asba_search_next_block(desc, BTAB_STATUS_FULL);
}

/* Read functions */

int asba_read_block(sAsba_desc_t *desc, void *dest_buffer, uint32_t blockno)
{
    int status=STATUS_OK;
    void *bp=asba_get_block_address(desc, blockno);
    memcpy(dest_buffer,bp, (size_t)desc->hdesc->bs );
    asba_update_current_block(desc, blockno);
    return status;
}

int asba_read_blockno(sAsba_desc_t *desc, void *dest_buffer, uint32_t blockno)
{
    int status=STATUS_OK; 
    if((status=asba_check_descriptor(desc) ) == STATUS_OK)
        status=asba_read_block(desc, dest_buffer, blockno);    
    return status;
}

int asba_read_current_block(sAsba_desc_t *desc, void *dest_buffer)
{
    int status=STATUS_OK;
    if((status=asba_check_descriptor(desc) ) == STATUS_OK)
    {
         if(asba_is_archive_empty(desc))
        {
            status=ERROR_READ_EMPTY;
        }
        else
        {
            if( (status=asba_search_next_used_block(desc)) != ERROR_NOBLOCK)
               status=asba_read_block(desc, dest_buffer,desc->curr); 
        }
    }
    return status;
}



int asba_write_block(sAsba_desc_t *desc, void *src_buffer, uint32_t blockno)
{
    memcpy(asba_get_block_address(desc, blockno), src_buffer, (size_t)desc->hdesc->bs );
    if((desc->tdesc.usef[blockno] & BTAB_STATUS_FULL) != BTAB_STATUS_FULL)
    {
        desc->tdesc.usef[desc->curr]|=BTAB_STATUS_FULL;
        desc->hdesc->used++;
    }
    msync(desc->map, (desc->hdesc->bs * desc->hdesc->bn), MS_ASYNC );
    return STATUS_OK;
}

int asba_write_blockno(sAsba_desc_t *desc, void *src_buffer, uint32_t blockno)
{
    int status=STATUS_OK; 
    if((status=asba_check_descriptor(desc) ) == STATUS_OK)
        status=asba_write_block(desc, src_buffer, blockno);    
    return status;
}


int asba_write_current_block(sAsba_desc_t *desc, void *src_buffer)
{
    int status=STATUS_OK;
    uint32_t uBlock=0; 

    if((status=asba_check_descriptor(desc) ) == STATUS_OK)
    {
         if(asba_is_archive_full(desc))
            status=ERROR_WRITE_FULL;
        else
        {
            if ((uBlock= asba_search_next_free_block2(desc, BTAB_STATUS_EMPTY)) >= 0)
            {
                status=asba_write_block(desc, src_buffer,uBlock);
                desc->curr=uBlock;
            } 
        }
    }
   return status;
}

int asba_delete_block(sAsba_desc_t *desc, uint32_t blockno)
{
    memset(asba_get_block_address(desc, blockno), 0xFF, (size_t)desc->hdesc->bs);
    if((desc->tdesc.usef[blockno] & BTAB_STATUS_FULL) == BTAB_STATUS_FULL)
    {
        desc->tdesc.usef[blockno]&= ~BTAB_STATUS_FULL;
        desc->hdesc->used--;
    }
    return STATUS_OK;
}

