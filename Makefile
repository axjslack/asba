# Makefile for ASBA Another (or Alexjan, but only when it works fine) Small Block Archive
# Alexjan Carraturo


CC=$(CROSS_COMPILE)gcc
AR=$(CROSS_COMPILE)ar
DBGOPT=-D DBG -ggdb

CFLAGS := -L. -I. -O0 -g $(DBGOPT) -Wall

LIB=asba
LIB_SRC := asba.c asba_op.c asba_pr.c
LIBFLAGS := -fPIC -shared -Wl,-soname,lib$(LIB).so
LIB_OBJ := asba.o

TLDFLAGS := -lasba
TCFLAGS  :=-L. -I. $(DBGOPT)
SRC := test.c
OBJS := test.o
TEST := test



all : $(LIB) $(TEST)

$(LIB) : $(LIB_SRC)
	$(CC) $(CFLAGS) $(LIBFLAGS) $^ -o lib$@.so
	ln -s lib$(LIB).so lib$(LIB).so.1

$(OBJS) : $(SRC)
	$(CC) $(TCFLAGS) -c $^ -o $@
$(TEST) : $(OBJS) 
	$(CC) $(TCFLAGS) $^ -o $@ $(TLDFLAGS) 

clean: 
	rm -rfv *.o
	rm -rfv *.a
	rm -rfv lib$(LIB).*
	rm -rfv $(TEST)
